# -*- mode: python ; coding: utf-8 -*-
from pathlib import Path

block_cipher = None

a = Analysis(['app.py'],
             pathex=[str(Path('.').absolute())],
             binaries=[],
             datas=[],
             hiddenimports=['pkg_resources.py2_warn'],
             hookspath=[],
             runtime_hooks=[],
             excludes=['pyinstaller'],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)

pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)

windivert = [('WinDivert64.dll','windivert/WinDivert64.dll', 'BINARY'), ('WinDivert64.sys','windivert/WinDivert64.sys', 'BINARY'),('WinDivert32.dll','windivert/WinDivert32.dll', 'BINARY'), ('WinDivert32.sys','windivert/WinDivert32.sys', 'BINARY')]

exe = EXE(pyz,
          a.scripts,
          a.binaries + windivert,
          a.zipfiles,
          a.datas,
          [],
          name='Victoria',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True )