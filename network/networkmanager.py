import requests
import pysher
import app
import json
import logging
import data

logger = logging.getLogger('victoria')
logger.propagate = False
logger.setLevel(logging.INFO)
if not logger.handlers:
    fh = logging.FileHandler(filename='history.log')
    fh.setLevel(logging.INFO)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)
    logger.addHandler(fh)


class Cloud:
    def __init__(self, token=None):
        self.config = data.ConfigData(data.file_name)
        self.token = token

    def _send_request(self, method, endpoint, params=None, payload=None, **kwargs):
        s = requests.Session()
        resp, resp_text = None, None
        url = self.config.get('api_url').format(endpoint)
        headers = {'User-Agent': 'Victoria/{})'.format(app.version),
                   'Content-Type': 'application/json; charset=UTF-8',
                   'Accept': 'application/json; charset=UTF-8',
                   'Authorization': 'Bearer ' + self.token if self.token else None}
        for _ in range(3):
            resp = s.request(method=method, url=url, params=params, json=payload, headers=headers, **kwargs)
            if resp.status_code != 502:  # Retry on error 502 "Bad Gateway"
                break

        if resp.status_code >= 404 and resp.status_code != 422:
            raise ConnectionError

        try:
            resp_text = resp.json(encoding="utf-8")
        except ValueError:
            resp_text = resp.text

        return resp.status_code, resp_text

    def auth(self, email, password):
        try:
            code, r = self._send_request('POST', 'authenticate', payload={'email': email, 'password': password})
            self.token = r.get('token')
            error = r.get('message') if code != 200 else None
            return self.token, error
        except (ConnectionError, AttributeError):
            return None, None

    def check_connection(self):
        try:
            code, r = self._send_request('GET', 'gateway/')
            if code == 200:
                return True
            elif code == 401:
                return False
            else:
                return None
        except (ConnectionError, AttributeError):
            return None

    def create_gateway(self, password=None):
        try:
            code, r = self._send_request('POST', 'gateway/create', payload={'password': password})
            error = r.get('message') if code != 200 else None
            return r.get('id'), error
        except (ConnectionError, AttributeError):
            return None, None

    def delete_gateway(self, gateway_id=None):
        try:
            code, r = self._send_request('DELETE', 'gateway/delete', payload={'id': gateway_id})
            error = r.get('message') if code != 200 else None
            return code == 200, error
        except (ConnectionError, AttributeError):
            return None, None

    def join_gateway(self, gateway_id, password=None):
        try:
            code, r = self._send_request('POST', 'gateway/join/' + gateway_id, payload={'password': password})
            error = "Gateway doesn't exist" if code == 404 else None
            if code == 422:
                error = r.get('errors', {}).get('user', r.get('message'))
            return code == 200, error[0] if type(error) == list else error
        except (ConnectionError, AttributeError):
            return None, None

    def leave_gateway(self):
        try:
            code, r = self._send_request('POST', 'gateway/leave')
            error = r.get('errors', {}).get('user', r.get('message')) if code != 200 else None
            return code == 200, error[0] if type(error) == list else error
        except (ConnectionError, AttributeError):
            return None, None

    def sync_ip(self):
        try:
            code, r = self._send_request('PUT', 'gateway/sync-ip')
            return code == 200
        except (ConnectionError, AttributeError):
            return None


class GatewaySocket:
    pusher_app_key = 'ftC480G4r8yVG2bSCci91paNZkKKPMMH'

    def __init__(self, token, connection_callback=None):
        self.config = data.ConfigData(data.file_name)
        self.connection_callback = connection_callback
        self.token = token
        self.pusher = pysher.Pusher(self.pusher_app_key, secure=True, custom_host=self.config.get('ws_endpoint'))
        self.pusher.connection.bind('pusher:connection_established', self._connect_handler)
        self.socket_id = None
        self.channel = None

    def _auth_request(self, params=None, payload=None, **kwargs):
        s = requests.Session()
        resp, resp_text = None, None
        headers = {'User-Agent': 'Victoria/{})'.format(app.version),
                   'Content-Type': 'application/json; charset=UTF-8',
                   'Authorization': 'Bearer ' + self.token if self.token else None}

        for _ in range(3):
            resp = s.request(method='POST', url=self.config.get('ws_auth_url'), params=params, json=payload, headers=headers, **kwargs)
            if resp.status_code != 502:  # Retry on error 502 "Bad Gateway"
                break

        if resp.status_code >= 400:
            raise ConnectionError

        try:
            resp_text = resp.json(encoding="utf-8")
        except ValueError:
            resp_text = resp.text

        return resp.status_code, resp_text

    def _auth_channel(self, gateway_id):
        code, resp = self._auth_request(payload={
            'socket_id': self.socket_id,
            'channel_name': 'private-app.victoria.gateway.{}'.format(gateway_id)
        })
        return resp.get('auth') if code == 200 else None

    def _connect_handler(self, data):
        logger.info("Websocket connected")
        data = json.loads(data)
        self.socket_id = data.get('socket_id')
        self.connection_callback(self)

    def connect(self):
        # nginx config on server set to timeout after 5min
        self.pusher.connection.ping_interval = 290
        self.pusher.connect()

    def disconnect(self):
        self.pusher.unsubscribe(self.channel)
        self.pusher.disconnect()
        logger.info("Websocket disconnected")

    def join_channel(self, gateway_id, event_handler):
        if self.socket_id is not None:
            auth = self._auth_channel(gateway_id)
            if auth:
                self.channel = self.pusher.subscribe('private-app.victoria.gateway.{}'.format(gateway_id), auth=auth)
                self.channel.bind('gateway.updated', event_handler)
                return True
            else:
                return False
        else:
            return None
