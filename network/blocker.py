import multiprocessing
import pydivert
import re
import logging
import data
from enum import Enum
from network import networkmanager
from app import IPValidator
from questionary import ValidationError

debug_logger = logging.getLogger('debugger')
debug_logger.setLevel(logging.DEBUG)
if not debug_logger.handlers:
    fh = logging.FileHandler('debugger.log')
    fh.setLevel(logging.DEBUG)
    fh.setFormatter(logging.Formatter('%(asctime)s|%(levelname)s: %(message)s', datefmt='%Y-%m-%d %H:%M:%S'))
    debug_logger.addHandler(fh)

ip_filter = re.compile(r'^(185\.56\.6[4-7]\.\d{1,3})$')
logger = logging.getLogger('victoria')
packet_filter = "(udp.SrcPort == 6672 or udp.DstPort == 6672) and ip"


class IpSet(object):
    def __init__(self):
        self.active = False
        self.ips = set()

    def activate(self):
        self.active = True

    def all(self):
        return self.ips

    def clear(self):
        self.ips = set()
        self.active = False

    def has(self, item):
        return item in self.ips

    def is_active(self):
        return self.active

    def override(self, items):
        self.ips = items
        self.active = True

    def union(self, items):
        self.ips.union(items)
        self.active = True


class Filter(object):
    """
    Packet filter that will allow packets from with source ip present on ips list
    """

    class Actions(Enum):
        """
        Possible packet filter options
        """
        ALLOW = 0
        DROP = 1

    def __init__(self, ips, action, name):
        """
        :param list ips: IP list to take action on
        :param Actions action: Action to take on the ips from the list
        :param str name: Name of the filtering process
        """
        self.ips = ips
        self.action = action
        self.name = name
        self.process = None
        self.exit = multiprocessing.Event()

    def start(self):
        self.process = multiprocessing.Process(target=self.run, args=(self.ips,))
        self.process.daemon = True
        self.process.start()
        logger.info('Dispatched {} blocker process'.format(self.name))

    def stop(self):
        self.process.terminate()
        self.process.join()
        logger.info('Terminated {} blocker process'.format(self.name))

    def run(self, ips):
        if not pydivert.WinDivert.is_registered():
            pydivert.WinDivert.register()
        try:
            with pydivert.WinDivert(packet_filter) as w:
                for packet in w:
                    ip = packet.ip.src_addr
                    if self.action == self.Actions.ALLOW:
                        if ip_filter.match(ip):
                            w.send(packet)
                        elif ips.has(ip):
                            w.send(packet)
                    elif self.action == self.Actions.DROP:
                        if not ips.has(ip):
                            w.send(packet)
        except KeyboardInterrupt:
            pass


class Whitelist(Filter):
    """
    Packet filter that will allow packets from with source ip present on ips list
    """
    def __init__(self, ips):
        super().__init__(ips, Filter.Actions.ALLOW, 'whitelist')


class Blacklist(Filter):
    """
    Packet filter that will block packets from with source ip present on ips list
    """
    def __init__(self, ips):
        super().__init__(ips, Filter.Actions.DROP, 'blacklist')


class IPSyncer(object):
    """
    Looper thread to update user ip to the cloud and domain based list items ips
    """
    def __init__(self, token, ip_set):
        """
        :param token: Cloud api token
        """
        self.ip_set = ip_set
        self.token = token
        self.process = multiprocessing.Process(target=self.run, args=(ip_set,))
        self.exit = multiprocessing.Event()

    def start(self):
        if self.token:
            self.process.start()
        else:
            logger.warning("Tried to start IPSyncer without token")

    def stop(self):
        if self.token:
            self.exit.set()
            self.process.join()

    def run(self, ip_set):
        while not self.exit.is_set():
            try:
                conn = networkmanager.Cloud(self.token)
                check = conn.check_connection()
                if check and ip_set.is_active():
                    conn.sync_ip()
                    logger.info("Ip Synced")
                config = data.ConfigData(data.file_name)
                lists = [data.CustomList('blacklist'), data.CustomList('custom_ips')]
                for l in lists:
                    outdated = []
                    new = {}
                    for ip, item in l:
                        domain = item.get('value')
                        if domain:
                            try:
                                ip_calc = IPValidator.validate_get(domain)
                                if ip != ip_calc:
                                    outdated.append(ip)
                                    new[ip_calc] = item
                            except ValidationError as e:
                                logger.warning(e.message)
                                continue
                    for old, new, item in zip(outdated, new.keys(), new.values()):
                        l.delete(old)
                        l.add(new, item)
                        logger.info("Updated {} ip".format(item.get('name', 'Unknown')))
                config.save()
                self.exit.wait(300)
            except KeyboardInterrupt:
                pass


class Debugger(object):
    """
    Thread to create a log of the ips matching the packet filter
    """
    def __init__(self, ips):
        self.ips = ips
        self.process = multiprocessing.Process(target=self.run, args=())
        self.process.daemon = True

    def start(self):
        self.process.start()

    def stop(self):
        self.process.terminate()

    def run(self):
        debug_logger.debug('Started debugging')
        with pydivert.WinDivert(packet_filter) as w:
            for packet in w:
                dst = packet.ip.dst_addr
                src = packet.ip.src_addr
                reserved = False
                whitelisted = False
                if ip_filter.match(dst) or ip_filter.match(src):
                    reserved = True
                elif dst in self.ips or src in self.ips:
                    whitelisted = True

                if reserved:
                    filler = 'Reserved'
                elif whitelisted:
                    filler = 'Whitelist'
                else:
                    filler = 'Blocked'

                if packet.is_inbound:
                    log = '[{}] {}:{} --> {}:{}'.format(filler, src, packet.src_port, dst, packet.dst_port)
                else:
                    log = '[{}] {}:{} <-- {}:{}'.format(filler, src, packet.src_port, dst, packet.dst_port)
                debug_logger.debug(log)
                w.send(packet)


class IPCollector(object):
    """
    Thread to store all the ip addresses matching the packet filter
    """
    def __init__(self):
        self.process = multiprocessing.Process(target=self.run, args=())
        self.process.daemon = True
        self.ips = multiprocessing.Manager().list()

    def start(self):
        self.process.start()
        logger.info('Dispatched ipcollector process')

    def stop(self):
        self.process.terminate()
        logger.info('Terminated ipcollector process')
        logger.info('Collected a total of {} IPs'.format(len(self.ips)))

    def run(self):
        with pydivert.WinDivert(packet_filter) as w:
            for packet in w:
                dst = packet.ip.dst_addr
                src = packet.ip.src_addr

                if packet.is_inbound:
                    self.ips.append(src)
                else:
                    self.ips.append(dst)
                w.send(packet)
