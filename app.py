from questionary import Validator, ValidationError, prompt, confirm
from prompt_toolkit.styles import Style
import os
import ctypes
from colorama import Fore
from network.blocker import *
import pydivert
import sys
import multiprocessing
from multiprocessing.managers import BaseManager
import ipaddress
from network import networkmanager
import socket
from tqdm import tqdm
import zipfile
import json
import time
import logging
import urllib.request

logger = logging.getLogger('victoria')
logger.propagate = False
logger.setLevel(logging.INFO)
if not logger.handlers:
    fh = logging.FileHandler(filename='history.log')
    fh.setLevel(logging.INFO)
    formatter = logging.Formatter('[%(asctime)s][%(levelname)s] %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
    fh.setFormatter(formatter)
    logger.addHandler(fh)

LF_FACESIZE = 32
STD_OUTPUT_HANDLE = -11
MAIN_COLOR = Fore.LIGHTGREEN_EX

version = '0.0.3'

ipv4 = re.compile(r"\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}\b")
domain = re.compile(r"^[a-z]+([a-z0-9-]*[a-z0-9]+)?(\.([a-z]+([a-z0-9-]*[a-z0-9]+)?)+)*$")

style = Style([
    ('qmark', 'fg:#16c60c bold'),  # token in front of the question
    ('question', 'bold'),  # question text
    ('answer', 'fg:#16c60c bold'),  # submitted answer text behind the question
    ('pointer', 'fg:#16c60c bold'),  # pointer used in select and checkbox prompts
    ('selected', 'fg:#FFFFFF bold'),  # style for a selected item of a checkbox
    ('separator', 'fg:#16c60c'),  # separator in lists
    ('instruction', '')  # user instructions for select, rawselect, checkbox
])


def print_white(msg):
    print(Fore.LIGHTWHITE_EX + msg + Fore.RESET)


def get_public_ip():
    public_ip = urllib.request.urlopen('https://ident.me').read().decode('utf8')
    if public_ip:
        logger.debug('Got a public IP')
        return public_ip
    else:
        logger.warning('Failed to get public IP')
        return False


def get_private_ip():
    soc = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    soc.connect(("8.8.8.8", 80))
    local_ip = soc.getsockname()[0]
    soc.close()
    return local_ip


def gateway_updated(data='{}'):
    """
    Handler for the gateway.updated ws event and repopulate the ip set for the blocker
    :param str data: JSON formatted string with `ips` field
    :return: None
    """
    global global_ip_set, custom_ips
    try:
        data = json.loads(data)
        data = set(data.get('ips', []))
    except json.decoder.JSONDecodeError:
        data = set()

    ip_set = global_ip_set.all()

    local_ip = get_private_ip()
    data.add(local_ip)

    public_ip = get_public_ip()
    if public_ip:
        data.add(public_ip)
    else:
        print_white('Failed to get Public IP. Running without.')

    for ip, friend in custom_ips:
        if friend.get('enabled'):
            try:
                ip_calc = IPValidator.validate_get(ip)
                data.add(ip_calc)
            except ValidationError:
                logger.warning('Not valid IP or URL: {}'.format(ip))
                print_white('Not valid IP or URL: "' +
                            MAIN_COLOR + '{}'.format(ip) +
                            Fore.LIGHTWHITE_EX + '"')
                continue

    if len(ip_set) > 0:
        diff = set()
        action = ''
        if len(data) >= len(ip_set):
            diff = data.difference(ip_set)
            action = ('joined', 'new')
        else:
            diff = ip_set.difference(data)
            action = ('left', 'deleted')
        if len(diff) > 0:
            logger.info("User {} gateway - updated ip set with [{}] {} ips".format(
                action[0],
                ','.join([ip for ip in diff]),
                action[1]
            ))
        else:
            logger.info("User joined/left the gateway from a whitelisted ip")
    global_ip_set.override(data)


def filter_packets(packet_filter, name=None, work_dur=10, sleep_dur=15, accent=MAIN_COLOR, propagate_exception=False):
    """
    Method to control package blocking loops
    :param packet_filter: Blacklist/Whitelist object
    :param str name: Name for logging and printing purposes, method will give no output if None
    :param int work_dur: Time in seconds the filter should run
    :param int sleep_dur: Time in seconds the filter should stop, method won't loop if None
    :param int accent: Color accent for printing purposes
    :param bool propagate_exception: Flag to enable the propagation of possible exceptions
    :return: None
    """
    global global_ip_set
    global_ip_set.activate()
    try:
        global config
        if config.get('continuous_block', False) and sleep_dur is not None:
            packet_filter.start()
            while True:
                pass
        else:
            while True:
                packet_filter.start()
                time.sleep(work_dur)
                packet_filter.stop()
                if sleep_dur is None:
                    break
                time.sleep(sleep_dur)
    except KeyboardInterrupt as e:
        packet_filter.stop()
        if name is not None:
            logger.info('Stopped {} session'.format(name))
            print_white('Stopped: "' +
                        accent + name.capitalize() + ' session' +
                        Fore.LIGHTWHITE_EX + '"')
        if propagate_exception:
            raise e
    finally:
        global_ip_set.clear()


class NameInCustom(Validator):
    def validate(self, document):
        global custom_ips
        if custom_ips.has(document.text):
            raise ValidationError(
                message='Name already in list',
                cursor_position=len(document.text))  # Move cursor to end


class NameInBlacklist(Validator):
    def validate(self, document):
        global blacklist
        if blacklist.has(document.text):
            raise ValidationError(
                message='Name already in list',
                cursor_position=len(document.text))  # Move cursor to end


class IPValidator(Validator):
    def validate(self, document):
        error = ValidationError(message='Not a valid IP or URL',
                                cursor_position=len(document.text))  # Move cursor to end
        try:
            ip = document.text
            if ipv4.match(ip):
                ipaddress.IPv4Address(ip)
            elif not domain.match(ip):
                raise error
        except (ipaddress.AddressValueError, socket.gaierror):
            raise error

    @staticmethod
    def validate_get(text):
        error = ValidationError(message='Not a valid IP or URL',
                                cursor_position=len(text))  # Move cursor to end
        try:
            ip = text
            if ipv4.match(ip):
                ipaddress.IPv4Address(ip)
            elif domain.match(ip):
                ip = socket.gethostbyname(text)
                ipaddress.IPv4Address(ip)
            else:
                raise error
            return ip
        except ipaddress.AddressValueError:
            raise error
        except socket.gaierror:
            raise ValidationError(message='URL {} can\'t be resolved to IP.'.format(text),
                                  cursor_position=len(text))  # Move cursor to end


class IPInCustom(IPValidator):
    def validate(self, document):
        super().validate(document)
        global custom_ips
        if document.text in custom_ips or custom_ips.has(document.text, 'value'):
            raise ValidationError(
                message='IP already in list',
                cursor_position=len(document.text)
            )  # Move cursor to end


class IPInBlacklist(Validator):
    def validate(self, document):
        super().validate(document)
        global blacklist
        if document.text in blacklist or blacklist.has(document.text, 'value'):
            raise ValidationError(
                message='IP already in list',
                cursor_position=len(document.text)
            )  # Move cursor to end


class ValidateToken(Validator):
    def validate(self, document):
        conn = networkmanager.Cloud(document.text)
        check = conn.check_connection()
        if check is None:
            raise ValidationError(
                message='DigitalArc is unavailable, unable to check token',
                cursor_position=len(document.text))  # Move cursor to end

        if check is False:
            raise ValidationError(
                message='Token invalid',
                cursor_position=len(document.text))  # Move cursor to end


def main():
    global cloud, config, custom_ips, blacklist, friends, global_ip_set
    while True:
        token = config.get('token')
        cloud.token = token
        check = cloud.check_connection()
        if check is not None:
            logger.info('Cloud online')
            print_white('Cloud service online')

            if token is not None:
                if check:
                    logger.debug('Valid token')
                    print_white('Valid token')
                else:
                    logger.debug('Invalid token')
                    print_white('Invalid token')
        else:
            logger.info('Cloud offline.')
            print_white('Cloud service down')

        options = {
            'type': 'list',
            'name': 'option',
            'message': 'What do you want?',
            'qmark': '@',
            'choices': [
                {
                    'name': 'Solo session',
                    'value': 'solo'
                },
                {
                    'name': 'Gateway',
                    'value': 'gateway'
                },
                {
                    'name': 'Whitelisted session',
                    'value': 'whitelist',
                },
                {
                    'name': 'Blacklisted session',
                    'value': 'blacklist',
                },
                {
                    'name': 'Auto whitelisted session',
                    'value': 'auto_whitelist',
                },
                {
                    'name': 'Kick unknowns',
                    'value': 'kick'
                },
                {
                    'name': 'New session',
                    'value': 'new'
                },
                {
                    'name': 'Lists',
                    'value': 'lists'
                },
                {
                    'name': 'Kick by IP',
                    'value': 'kick_by_ip'
                },
                {
                    'name': 'Options',
                    'value': 'options'
                },
                {
                    'name': 'Support zip',
                    'value': 'support_zip'
                },
                {
                    'name': 'Quit',
                    'value': 'quit'
                }
            ]
        }
        if not token or not check:
            options['choices'][1]['disabled'] = "No token"

        answer = prompt(options, style=style)
        if not answer:
            if pydivert.WinDivert.is_registered():
                pydivert.WinDivert.unregister()
            sys.exit(0)
        os.system('cls')
        option = answer['option']

        if option == 'solo':
            logger.info('Starting solo session')
            print_white('Running: "' +
                        MAIN_COLOR + 'Solo session' +
                        Fore.LIGHTWHITE_EX + '" Press "' + MAIN_COLOR + 'CTRL + C' +
                        Fore.LIGHTWHITE_EX + '" to stop.')
            global_ip_set.override(set())
            packet_filter = Whitelist(ips=global_ip_set)
            filter_packets(packet_filter, name='solo')

        elif option == 'gateway':
            while True:
                options = {
                    'type': 'list',
                    'name': 'option',
                    'qmark': '@',
                    'message': 'What do you want?',
                    'choices': [
                        {
                            'name': 'Create',
                            'value': 'create'
                        },
                        {
                            'name': 'Join',
                            'value': 'join'
                        },
                        {
                            'name': 'MainMenu',
                            'value': 'return'
                        }
                    ]
                }
                answer = prompt(options, style=style)
                if not answer or answer['option'] == 'return':
                    os.system('cls')
                    break

                elif answer['option'] == 'create':
                    os.system('cls')
                    options = {
                        'type': 'input',
                        'name': 'password',
                        'qmark': '@',
                        'message': 'Enter the gateway pass (leave empty for none):'
                    }
                    answer = prompt(options, style=style)
                    if not answer:
                        os.system('cls')
                        continue
                    password = answer['password']
                    password = password if password != '' else None
                    gateway_id, error = cloud.create_gateway(password)
                    if gateway_id is not None:
                        logger.info("Gateway created {}".format(gateway_id))
                        gws = networkmanager.GatewaySocket(config.get('token'), lambda gws: gws.join_channel(gateway_id, gateway_updated))
                        try:
                            gws.connect()

                            gateway_updated()
                            print_white('Gateway created: "' +
                                        MAIN_COLOR + gateway_id +
                                        Fore.LIGHTWHITE_EX + '" Press "' + MAIN_COLOR + 'CTRL + C' +
                                        Fore.LIGHTWHITE_EX + '" to stop.')

                            packet_filter = Whitelist(ips=global_ip_set)
                            filter_packets(packet_filter, name='gateway', propagate_exception=True)
                        except KeyboardInterrupt:
                            pass
                        finally:
                            gws.disconnect()

                            res, error = cloud.delete_gateway(gateway_id)
                            if res:
                                logger.info("Deleted gateway {}".format(gateway_id))
                            elif error:
                                logger.warning("Error deleting gateway {}: {}".format(gateway_id, error))
                            else:
                                logger.error("Unable to connect with cloud (delete gateway)")
                    elif error:
                        logger.warning(error)
                        print_white(error)
                    else:
                        logger.error("Unable to connect with cloud (create gateway)")
                        print_white("Unable to connect with cloud")

                elif answer['option'] == 'join':
                    os.system('cls')
                    options = [
                        {
                            'type': 'input',
                            'name': 'gateway',
                            'qmark': '@',
                            'message': 'Enter the gateway ID:'
                        },
                        {
                            'type': 'password',
                            'name': 'password',
                            'qmark': '@',
                            'message': 'Enter the gateway pass (leave empty if none):'
                        }
                    ]
                    answer = prompt(options, style=style)
                    if not answer:
                        os.system('cls')
                        continue
                    gateway_id = answer['gateway']
                    password = answer['password']
                    password = password if password != '' else None
                    try:
                        check, error = cloud.join_gateway(gateway_id, password)
                        if check:
                            # Tweak to avoid creating another process shared variable
                            # the active flag is used to activate the IpSyncer own ip sync
                            global_ip_set.activate()
                            logger.info("Joined gateway")
                            print_white('Joined gateway: "' +
                                        MAIN_COLOR + gateway_id +
                                        Fore.LIGHTWHITE_EX + '" Press "' + MAIN_COLOR + 'CTRL + C' +
                                        Fore.LIGHTWHITE_EX + '" to leave it.')
                            while True:
                                time.sleep(300)
                        elif error:
                            print_white(error)
                            logger.warning(error)
                            time.sleep(1.5)
                        else:
                            logger.error("Unable to connect with cloud (join gateway)")

                    except KeyboardInterrupt:
                        pass
                    finally:
                        # Deactivate IpSyncer own ip sync
                        global_ip_set.clear()
                        check, error = cloud.leave_gateway()
                        if check:
                            logger.info("Left gateway")
                        elif error:
                            logger.warning(error)
                        else:
                            logger.error("Unable to connect with cloud (leave gateway)")

                os.system('cls')

        elif option == 'whitelist':
            local_ip = get_private_ip()
            ip_set = {local_ip}
            public_ip = get_public_ip()
            if public_ip:
                ip_set.add(public_ip)
            else:
                print_white('Failed to get Public IP. Running without.')

            for ip, friend in custom_ips:
                if friend.get('enabled'):
                    try:
                        ip_calc = IPValidator.validate_get(ip)
                        ip_set.add(ip_calc)
                    except ValidationError:
                        logger.warning('Not valid IP or URL: {}'.format(ip))
                        print_white('Not valid IP or URL: "' +
                                    MAIN_COLOR + '{}'.format(ip) +
                                    Fore.LIGHTWHITE_EX + '"')
                        continue

            for ip, friend in friends:
                if friend.get('enabled'):
                    ip_set.add(ip)

            logger.info('Starting whitelisted session with {} IPs'.format(len(ip_set)))
            print_white('Running: "' +
                        MAIN_COLOR + 'Whitelisted session' +
                        Fore.LIGHTWHITE_EX + '" Press "' +
                        MAIN_COLOR + 'CTRL + C' +
                        Fore.LIGHTWHITE_EX + '" to stop.')
            global_ip_set.override(ip_set)
            packet_filter = Whitelist(ips=global_ip_set)
            filter_packets(packet_filter, name='whitelisted')

        elif option == 'blacklist':
            ip_set = set()
            for ip, item in blacklist:
                if item.get('enabled'):
                    try:
                        ip = IPValidator.validate_get(ip)
                        ip_set.add(ip)
                    except ValidationError:
                        logger.warning('Not valid IP or URL: {}'.format(ip))
                        print_white('Not valid IP or URL: "' +
                                    MAIN_COLOR + '{}'.format(ip) +
                                    Fore.LIGHTWHITE_EX + '"')
                        continue
            logger.info('Starting blacklisted session with {} IPs'.format(len(ip_set)))
            print_white('Running: "' +
                        Fore.LIGHTBLACK_EX + 'Blacklist' +
                        Fore.LIGHTWHITE_EX + '" Press "' +
                        Fore.LIGHTBLACK_EX + 'CTRL + C' +
                        Fore.LIGHTWHITE_EX + '" to stop.')
            global_ip_set.override(ip_set)
            packet_filter = Blacklist(ips=global_ip_set)
            filter_packets(packet_filter, name='blacklisted', accent=Fore.LIGHTBLACK_EX)

        elif option == 'auto_whitelist':
            logger.info('Starting auto whitelisted session')
            collector = IPCollector()
            logger.info('Starting to collect IPs')
            collector.start()
            for _ in tqdm(range(10), ascii=True, desc='Collecting session'):
                time.sleep(1)
            collector.stop()
            ip_set = set(collector.ips)
            logger.info('Collected {} IPs'.format(len(ip_set)))
            local_ip = get_private_ip()
            ip_set.add(local_ip)
            public_ip = get_public_ip()
            if public_ip:
                ip_set.add(public_ip)
            else:
                print_white('Failed to get Public IP. Running without.')

            for ip, friend in custom_ips:
                if friend.get('enabled'):
                    try:
                        ip_calc = IPValidator.validate_get(ip)
                        ip_set.add(ip_calc)
                    except ValidationError:
                        logger.warning('Not valid IP or URL: {}'.format(ip))
                        print_white('Not valid IP or URL: "' +
                                    MAIN_COLOR + '{}'.format(ip) +
                                    Fore.LIGHTWHITE_EX + '"')
                        continue

            for ip, friend in friends:
                if friend.get('enabled'):
                    ip_set.add(ip)

            os.system('cls')
            logger.info('Starting whitelisted session with {} IPs'.format(len(ip_set)))
            print_white('Running: "' +
                        MAIN_COLOR + 'Whitelisted session' +
                        Fore.LIGHTWHITE_EX + '" Press "' +
                        MAIN_COLOR + 'CTRL + C' +
                        Fore.LIGHTWHITE_EX + '" to stop.')
            global_ip_set.override(ip_set)
            packet_filter = Whitelist(ips=global_ip_set)
            filter_packets(packet_filter, name='whitelisted')

        elif option == 'lists':
            while True:
                options = {
                    'type': 'list',
                    'name': 'option',
                    'qmark': '@',
                    'message': 'What do you want?',
                    'choices': [
                        {
                            'name': 'Custom',
                            'value': 'custom'
                        },
                        {
                            'name': 'Blacklist',
                            'value': 'blacklist'
                        },
                        {
                            'name': 'MainMenu',
                            'value': 'return'
                        }
                    ]
                }
                answer = prompt(options, style=style)

                if not answer or answer['option'] == 'return':
                    os.system('cls')
                    break

                elif answer['option'] == 'custom':
                    os.system('cls')
                    while True:
                        options = {
                            'type': 'list',
                            'name': 'option',
                            'qmark': '@',
                            'message': 'Custom list',
                            'choices': [
                                {
                                    'name': 'Select',
                                    'value': 'select'
                                },
                                {
                                    'name': 'Add',
                                    'value': 'add'
                                },
                                {
                                    'name': 'List',
                                    'value': 'list'
                                },
                                {
                                    'name': 'MainMenu',
                                    'value': 'return'
                                }
                            ]
                        }
                        answer = prompt(options, style=style)

                        if not answer or answer['option'] == 'return':
                            os.system('cls')
                            break

                        elif answer['option'] == 'select':
                            os.system('cls')
                            if len(custom_ips) <= 0:
                                print_white('No friends')
                                continue
                            else:
                                c = [{
                                    'name': f.get('name'),
                                    'checked': True if f.get('enabled') else None
                                } for ip, f in custom_ips]
                                options = {
                                    'type': 'checkbox',
                                    'name': 'option',
                                    'qmark': '@',
                                    'message': 'Select who to enable',
                                    'choices': c
                                }
                                answer = prompt(options, style=style)
                                if not answer:
                                    os.system('cls')
                                    continue
                                for ip, item in custom_ips:
                                    item['enabled'] = item.get('name') in answer['option']
                                config.save()

                        elif answer['option'] == 'add':
                            os.system('cls')
                            options = [
                                {
                                    'type': 'input',
                                    'name': 'name',
                                    'message': 'Name',
                                    'qmark': '@',
                                    'validate': NameInCustom
                                },
                                {
                                    'type': 'input',
                                    'name': 'ip',
                                    'message': 'IP/URL',
                                    'qmark': '@',
                                    'validate': IPInCustom
                                },
                            ]

                            answer = prompt(options, style=style)
                            if not answer:
                                os.system('cls')
                                continue
                            try:
                                ip = IPValidator.validate_get(answer['ip'])
                                item = {
                                    'name': answer['name'],
                                    'enabled': True
                                }
                                if ip != answer['ip']:
                                    item['value'] = answer['ip']
                                custom_ips.add(ip, item)
                                config.save()
                            except ValidationError as e:
                                print_white(e.message)

                        elif answer['option'] == 'list':
                            os.system('cls')
                            while True:
                                if len(custom_ips) <= 0:
                                    print_white('No friends')
                                    break
                                else:
                                    c = [{
                                        'name': f.get('name'),
                                        'checked': True if f.get('enabled') else None
                                    } for ip, f in custom_ips]
                                    options = {
                                        'type': 'list',
                                        'name': 'name',
                                        'qmark': '@',
                                        'message': 'Select who to view',
                                        'choices': c
                                    }
                                    name = prompt(options, style=style)
                                    if not name:
                                        os.system('cls')
                                        break
                                    options = {
                                        'type': 'list',
                                        'name': 'option',
                                        'qmark': '@',
                                        'message': 'Select what to do',
                                        'choices': [
                                            {
                                                'name': 'Edit',
                                                'value': 'edit'
                                            },
                                            {
                                                'name': 'Delete',
                                                'value': 'delete'
                                            },
                                            {
                                                'name': 'Back',
                                                'value': 'return'
                                            }
                                        ]
                                    }
                                    name = name['name']
                                    answer = prompt(options, style=style)
                                    if not answer or answer['option'] == 'return':
                                        os.system('cls')
                                        break

                                    elif answer['option'] == 'edit':
                                        while True:
                                            print(
                                                'Notice, user deleted. Press enter to go back / Save. Quit and you lose him.')
                                            ip, item = custom_ips.find(name)
                                            entry = item.get('value', ip)
                                            custom_ips.delete(ip)
                                            config.save()
                                            options = [
                                                {
                                                    'type': 'input',
                                                    'name': 'name',
                                                    'message': 'Name',
                                                    'qmark': '@',
                                                    'validate': NameInCustom,
                                                    'default': name
                                                },
                                                {
                                                    'type': 'input',
                                                    'name': 'ip',
                                                    'message': 'IP/URL',
                                                    'qmark': '@',
                                                    'validate': IPInCustom,
                                                    'default': entry
                                                },
                                            ]

                                            answer = prompt(options, style=style)
                                            if not answer:
                                                os.system('cls')
                                                break
                                            try:
                                                ip = IPValidator.validate_get(answer['ip'])
                                                item['name'] = answer['name']
                                                item['enabled'] = True
                                                if ip != answer['ip']:
                                                    item['value'] = answer['ip']
                                                custom_ips.add(ip, item)
                                                config.save()
                                                os.system('cls')
                                            except ValidationError as e:
                                                custom_ips.add(ip, item)
                                                config.save()
                                                print_white('Original item was restored due to error: ' + e.message)
                                            break

                                    elif answer['option'] == 'delete':
                                        ip, item = custom_ips.find(name)
                                        custom_ips.delete(ip)
                                        config.save()

                elif answer['option'] == 'blacklist':
                    os.system('cls')
                    while True:
                        options = {
                            'type': 'list',
                            'name': 'option',
                            'qmark': '@',
                            'message': 'Blacklist',
                            'choices': [
                                {
                                    'name': 'Select',
                                    'value': 'select'
                                },
                                {
                                    'name': 'Add',
                                    'value': 'add'
                                },
                                {
                                    'name': 'List',
                                    'value': 'list'
                                },
                                {
                                    'name': 'MainMenu',
                                    'value': 'return'
                                }
                            ]
                        }
                        answer = prompt(options, style=style)

                        if not answer or answer['option'] == 'return':
                            os.system('cls')
                            break

                        elif answer['option'] == 'select':
                            os.system('cls')
                            if len(blacklist) <= 0:
                                print_white('No ips')
                                continue
                            else:
                                c = [{
                                    'name': f.get('name'),
                                    'checked': True if f.get('enabled') else None
                                } for ip, f in blacklist]
                                options = {
                                    'type': 'checkbox',
                                    'name': 'option',
                                    'qmark': '@',
                                    'message': 'Select who to enable',
                                    'choices': c
                                }
                                answer = prompt(options, style=style)
                                if not answer:
                                    os.system('cls')
                                    continue
                                for ip, item in blacklist:
                                    item['enabled'] = item.get('name') in answer['option']
                                config.save()

                        elif answer['option'] == 'add':
                            os.system('cls')
                            options = [
                                {
                                    'type': 'input',
                                    'name': 'name',
                                    'message': 'Name',
                                    'qmark': '@',
                                    'validate': NameInBlacklist
                                },
                                {
                                    'type': 'input',
                                    'name': 'ip',
                                    'message': 'IP/URL',
                                    'qmark': '@',
                                    'validate': IPInBlacklist
                                },
                            ]

                            answer = prompt(options, style=style)
                            if not answer:
                                os.system('cls')
                                continue
                            try:
                                ip = IPValidator.validate_get(answer['ip'])
                                item = {
                                    'name': answer['name'],
                                    'enabled': True
                                }
                                if ip != answer['ip']:
                                    item['value'] = answer['ip']
                                blacklist.add(ip, item)
                                config.save()
                            except ValidationError as e:
                                print_white(e.message)

                        elif answer['option'] == 'list':
                            os.system('cls')
                            while True:
                                if len(blacklist) <= 0:
                                    print_white('No items')
                                    break
                                else:
                                    c = [{
                                        'name': f.get('name'),
                                        'checked': True if f.get('enabled') else None
                                    } for ip, f in blacklist]
                                    options = {
                                        'type': 'list',
                                        'name': 'name',
                                        'qmark': '@',
                                        'message': 'Select who to view',
                                        'choices': c
                                    }
                                    name = prompt(options, style=style)
                                    if not name:
                                        os.system('cls')
                                        break
                                    options = {
                                        'type': 'list',
                                        'name': 'option',
                                        'qmark': '@',
                                        'message': 'Select what to do',
                                        'choices': [
                                            {
                                                'name': 'Edit',
                                                'value': 'edit'
                                            },
                                            {
                                                'name': 'Delete',
                                                'value': 'delete'
                                            },
                                            {
                                                'name': 'Back',
                                                'value': 'return'
                                            }
                                        ]
                                    }
                                    name = name['name']
                                    answer = prompt(options, style=style)
                                    if not answer or answer['option'] == 'return':
                                        os.system('cls')
                                        break

                                    elif answer['option'] == 'edit':
                                        while True:
                                            print(
                                                'Notice, user deleted. Press enter to go back / Save. Quit and you lose him.')
                                            ip, item = blacklist.find(name)
                                            blacklist.delete(ip)
                                            config.save()
                                            entry = item.get('value', ip)
                                            options = [
                                                {
                                                    'type': 'input',
                                                    'name': 'name',
                                                    'message': 'Name',
                                                    'qmark': '@',
                                                    'validate': NameInBlacklist,
                                                    'default': name
                                                },
                                                {
                                                    'type': 'input',
                                                    'name': 'ip',
                                                    'message': 'IP/URL',
                                                    'qmark': '@',
                                                    'validate': IPInBlacklist,
                                                    'default': entry
                                                },
                                            ]

                                            answer = prompt(options, style=style)
                                            if not answer:
                                                os.system('cls')
                                                break
                                            try:
                                                ip = IPValidator.validate_get(answer['ip'])
                                                item['name'] = answer['name']
                                                item['enabled'] = True
                                                if ip != answer['ip']:
                                                    item['value'] = answer['ip']
                                                blacklist.add(ip, item)
                                                config.save()
                                                os.system('cls')
                                            except ValidationError as e:
                                                blacklist.add(ip, item)
                                                config.save()
                                                print_white('Original item was restored due to error: ' + e.message)
                                            break

                                    elif answer['option'] == 'delete':
                                        ip, item = blacklist.find(name)
                                        blacklist.delete(ip)
                                        config.save()

        elif option == 'kick_by_ip':
            collector = IPCollector()
            collector.start()
            for _ in tqdm(range(10), ascii=True, desc='Collecting session'):
                time.sleep(1)
            collector.stop()
            ip_set = set(collector.ips)
            os.system('cls')
            if len(ip_set) <= 0:
                print_white('None')
                break
            options = {
                'type': 'checkbox',
                'name': 'option',
                'qmark': '@',
                'message': 'Select IP\'s to kick',
                'choices': [ip for ip in ip_set]
            }
            answer = prompt(options, style=style)
            if not answer:
                os.system('cls')
                break

            print_white('Running: "' +
                        Fore.LIGHTBLACK_EX + 'Blacklist' +
                        Fore.LIGHTWHITE_EX + '"')
            ip_set = set(answer['option'])
            global_ip_set.override(ip_set)
            packet_filter = Blacklist(ips=global_ip_set)
            filter_packets(packet_filter, sleep_dur=None)

        elif option == 'kick':
            local_ip = get_private_ip()
            ip_set = {local_ip}
            public_ip = get_public_ip()
            if public_ip:
                ip_set.add(public_ip)
            else:
                print_white('Failed to get Public IP. Running without.')
            for ip, friend in custom_ips:
                if friend.get('enabled'):
                    try:
                        ip_calc = IPValidator.validate_get(ip)
                        ip_set.add(ip_calc)
                    except ValidationError:
                        logger.warning('Not valid IP or URL: {}'.format(ip))
                        print_white('Not valid IP or URL: "' +
                                    MAIN_COLOR + '{}'.format(ip) +
                                    Fore.LIGHTWHITE_EX + '"')
                        continue

            for ip, friend in friends:
                if friend.get('enabled'):
                    ip_set.add(ip)
            print_white('Kicking unknowns')
            global_ip_set.override(ip_set)
            packet_filter = Whitelist(ips=global_ip_set)
            filter_packets(packet_filter, sleep_dur=None)
            continue

        elif option == 'new':
            print_white('Creating new session')
            global_ip_set.override(set())
            packet_filter = Whitelist(ips=global_ip_set)
            filter_packets(packet_filter, sleep_dur=None)
            continue

        elif option == 'options':
            while True:
                os.system('cls')
                options = {
                    'type': 'list',
                    'name': 'option',
                    'message': 'Options',
                    'qmark': '@',
                    'choices': [
                        {
                            'name': 'Authenticate',
                            'value': 'authenticate'
                        },
                        {
                            'name': 'Token',
                            'value': 'token'
                        },
                        {
                            'name': 'Continuous mode',
                            'value': 'continuous'
                        },
                        {
                            'name': 'MainMenu',
                            'value': 'return'
                        }
                    ]
                }
                answer = prompt(options, style=style)
                if not answer:
                    break
                os.system('cls')
                option = answer['option']

                if not answer or answer['option'] == 'return':
                    os.system('cls')
                    break

                elif option == 'authenticate':
                    try:
                        while True:
                            options = [
                                {
                                    'type': 'text',
                                    'name': 'email',
                                    'message': 'Enter your email'
                                },
                                {
                                    'type': 'password',
                                    'name': 'password',
                                    'message': 'Enter your password'
                                }
                            ]
                            answers = prompt(options, style=style)
                            token, error = cloud.auth(answers['email'], answers['password'])
                            if token:
                                config.set('token', token)
                                config.save()
                                print_white('Correct credentials')
                                time.sleep(1.5)
                                break
                            else:
                                print_white(error)
                    except KeyboardInterrupt:
                        pass
                    os.system('cls')

                elif option == 'token':
                    token = config.get('token')
                    options = {
                        'type': 'input',
                        'name': 'token',
                        'qmark': '@',
                        'message': 'Paste your token:',
                        'validate': ValidateToken
                    }
                    if token:
                        options['default'] = token
                    answer = prompt(options, style=style)
                    if not answer:
                        os.system('cls')
                        continue
                    config.set('token', answer['token'])
                    config.save()
                    os.system('cls')

                elif option == 'continuous':
                    options = {
                        'type': 'select',
                        'name': 'option',
                        'message': 'Enable?',
                        'qmark': '@',
                        'choices': [
                            {
                                'name': 'Yes',
                                'value': True,
                                'checked': config.get('continuous_block', False)
                            },
                            {
                                'name': 'No',
                                'value': False,
                                'checked': not config.get('continuous_block', False)
                            }
                        ]
                    }

                    answer = prompt(options, style=style)
                    if not answer:
                        continue
                    os.system('cls')
                    if answer['option']:
                        options = {
                            'type': 'confirm',
                            'name': 'agree',
                            'qmark': '@',
                            'message': 'This option might cause disconnections on low spec devices and is under testing, are you sure?'
                        }
                        answer = prompt(options, style=style)
                        config.set('continuous_block', answer['agree'])
                        config.save()

        elif option == 'support_zip':
            os.system('cls')
            print_white('NOTICE: This program will now log all udp traffic on port 6672 for 1 minute. '
                        'Only run this if you are okay with that.')
            options = {
                'type': 'confirm',
                'name': 'agree',
                'qmark': '@',
                'message': 'Agree?'
            }
            answer = prompt(options, style=style)
            if not answer:
                os.system('cls')
                continue
            if answer.get('agree'):
                local_list = config.get('custom_ips')
                cloud_list = config.get('friends')
                ip_set = []
                for friend in local_list:
                    if friend.get('enabled'):
                        try:
                            ip = IPValidator.validate_get(friend.get('ip'))
                            ip_set.append(ip)
                        except ValidationError:
                            continue
                for friend in cloud_list:
                    if friend.get('enabled'):
                        ip_set.append(friend.get('ip'))
                debugger = Debugger(ip_set)
                debugger.start()
                for _ in tqdm(range(60), ascii=True, desc='Collecting Requests'):
                    time.sleep(1)
                debugger.stop()
                time.sleep(1)
                print_white('Collecting data')
                token = config.get('token')
                print_white('Checking connections')
                runner = networkmanager.Cloud(token)
                check = runner.check_connection()
                da_status = 'Online' if check is not None else 'Offline'
                if token:
                    if check:
                        has_token = 'Has a valid token'
                    else:
                        has_token = 'Has a invalid token'
                else:
                    has_token = 'Does not have a token'

                datas = {
                    'token': has_token,
                    'da_status': da_status,
                    'customlist': custom_ips,
                    'cloud': friends
                }

                print_white('Writing data')
                with open("datacheck.json", "w+") as datafile:
                    json.dump(datas, datafile, indent=2)
                print_white('Packing debug request')
                compressed = zipfile.ZipFile('debugger-{}.zip'.format(time.strftime("%Y%m%d-%H%M%S")), "w",
                                             zipfile.ZIP_DEFLATED)
                compressed.write('datacheck.json')
                try:
                    compressed.write('debugger.log')
                except FileNotFoundError:
                    pass
                os.remove('datacheck.json')
                try:
                    os.remove('debugger.log')
                except FileNotFoundError:
                    pass
                print_white('Finished')
                compressed.close()
                continue
            else:
                print_white('Declined')
                continue

        elif option == 'quit':
            if pydivert.WinDivert.is_registered():
                pydivert.WinDivert.unregister()
            sys.exit(0)

        os.system('cls')


if __name__ == '__main__':
    multiprocessing.freeze_support()
    BaseManager.register('IpSet', IpSet)
    manager = BaseManager()
    manager.register('IpSet', IpSet)
    manager.start()
    global_ip_set = manager.IpSet()
    config = data.ConfigData(data.file_name)
    try:
        blacklist = data.CustomList('blacklist')
        custom_ips = data.CustomList('custom_ips')
        friends = data.CustomList('friends')
        if friends is not None:
            friends.destroy()
            config.save()
    except data.MigrationRequired:
        data.MigrationRequired.migrate_to_dict()
        time.sleep(5)
        sys.exit()

    os.system('cls')
    logger.info('Init')
    if not ctypes.windll.shell32.IsUserAnAdmin():
        print_white('Please start as administrator')
        logger.info('Started without admin')
        input('Press enter to exit.')
        sys.exit()
    logger.info('Booting up')
    print_white('Booting up...')
    if not pydivert.WinDivert.is_registered():
        pydivert.WinDivert.register()
    ctypes.windll.kernel32.SetConsoleTitleW('Victoria {}'.format(version))
    token = config.get('token')
    cloud = networkmanager.Cloud(token)
    ipsyncer = IPSyncer(None, global_ip_set)
    print_white('Checking connections.')
    check = cloud.check_connection()
    if check:
        ipsyncer.token = token
        ipsyncer.start()
        print_white('Starting IP syncer.')
    print(MAIN_COLOR + 'Welcome to Victoria.' + Fore.RESET)
    while True:
        try:
            main()
        except KeyboardInterrupt:
            continue
        finally:
            ipsyncer.stop()
